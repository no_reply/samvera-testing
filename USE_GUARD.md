Using Guard
===========

This repository ships with [Guard](https://github.com/guard/guard) and
[Guard RSpec](https://github.com/ranmocy/guard-rspec).

Running `bundle exec guard` will start a process that watches files for
changes and runs tests automatically!
