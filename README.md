Testing your Samvera Application
================================

This repository supports a workshop given at
[Samvera Connect 2018](https://wiki.duraspace.org/display/samvera/Samvera+Connect+2018).

This workshop builds on basic RSpec testing skills to help developers build robust test suites for Samvera applications. What should you test? How can you DRY up your test suite to streamline test development and maintenance? Developers will leave with an expanded sense of advanced testing tools and how to effectively use them in Samvera development.

## Prerequisites

**Please ensure you can do the following prior to the workshop:**

```sh
git clone https://gitlab.com/no_reply/samvera-testing.git
cd samvera-testing
bundle
bundle exec rake
```

### Prerequisites

If you have trouble with the above, please ensure the following are installed:

 * [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
 * Ruby 2.3+
   * You need a currently supported version of Ruby. If you don't already have this installed, please follow the Ruby installation [instructions](https://www.ruby-lang.org/en/documentation/installation/).
 * [Hyrax](https://github.com/samvera/hyrax#prerequisites) (_optional_)
   * Hyrax has various prerequisites; you may benefit from having all of these setup for a working Hyrax environment.

## Agenda

### Getting started with RSpec

The Samvera community has largely converged on using RSpec as a testing
framework. RSpec provides a DSL for rapid Test Driven Development.

Some RSpec Resources:
  * [RSpec Core Documentation](https://relishapp.com/rspec/rspec-core/docs)
  * [RSpec Expectations Documentation](https://relishapp.com/rspec/rspec-expectations/docs)
  * [RSpec Mocks Documentation](https://relishapp.com/rspec/rspec-mocks/docs)
  * [RSpec Rails Documentation](https://relishapp.com/rspec/rspec-rails/docs)
  * [BetterSpecs](http://www.betterspecs.org/)

#### Your first test

Let's write an for an `Animal`. Our `Animal` will have one method `Animal#speak`. Write a _Unit Test_ for a `#speak` method with the behavior below at `spec/animal_spec.rb`; then add the implementation to make it pass.

```ruby
# lib/animal.rb
class Animal
  ##
  # @return [String]
  def speak
    "Hello!"
  end
end
```

An example of a unit test for `#speak` is in the branch `exercise-1`.

> **Types of Tests**
>
> There are many (overlapping, sometimes contradictory) taxonomies of tests.
> The thing they share is categorization based on the **System Under Test**.
> For our purposes, we want to consider tests based on the _size_ or
> _complexity_ of what we are testing:
>
>  * [Unit Tests](https://martinfowler.com/bliki/UnitTest.html) concern
>    themselves with a single `Class` or `#method`.
>  * [Integration Tests](https://martinfowler.com/bliki/IntegrationTest.html)
>    test the interaction of multiple separate but inter-dependent units.
>
> This simple distinction subsumes and elides many other types of tests commonly
> talked about under many different names: _feature tests_, _regression tests_,
> _system tests_, _smoke tests_, _acceptance tests_; as well as the
> Rails-specific types found in
> [RSpec Rails](https://relishapp.com/rspec/rspec-rails/docs)

#### Having a conversation

Let's add a simple `Conversation` class. Our `Conversation` will generate a
brief exchange between two animals using the `#chat` method. Let's write a test
for this method, then add the implementation as below:

```ruby
# lib/conversation.rb
class Conversation
  attr_accessor :speakers

  ##
  # @param [#speak] speaker_1
  # @param [#speak] speaker_2
  def initialize(speaker_1, speaker_2)
    self.speakers = [speaker_1, speaker_2]
  end

  ##
  # @return [String]
  def chat
    "#{speakers[0].speak}\n#{speakers[1].speak}\n"
  end
end
```

An example of a test for `#chat` is in the branch `exercise-2`.

**Discussion Topics**

  * Is this a unit test, or an integration test?
  * What is the purpose of the string arguments to the `describe` and `it` blocks?
  * What's going on in the line beginning `subject(:conversation)`?

### Advanced RSpec
#### DRY Tests

  * Testing interfaces: `shared_examples`
  * Testing scenarios: `shared_context`
  * [One expectation per test?!](http://www.betterspecs.org/#single)

#### Test Doubles: Stubs, Mocks, Fakes

  * Real fakes
  * [Mocks aren't Stubs](https://martinfowler.com/articles/mocksArentStubs.html)
  * [RSpec Mock](https://relishapp.com/rspec/rspec-mocks/docs)

### Factories

  * [Getting Started with FactoryBot](https://github.com/thoughtbot/factory_bot/blob/master/GETTING_STARTED.md)

### Testing Samvera Applications

  * https://github.com/samvera-labs/hyrax-spec
